var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var requestJson = require('request-json');

  var path = require('path');

  var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=ncSplbUzyZHcoE-p5adUQc8XgOfaAcyZ";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.send('Hola mundo');
  //res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/movimientos', function(req, res){
  //res.send('Hola mundo');
  var clienteMlab = requestJson.createClient(urlMovimientosMlab);
  clienteMlab.get('', function(err, resM, body){
    if(err)
    {
      console.log(body);
    }
    else
    {
      res.send(body);
    }
  });
  //res.sendFile(path.join(__dirname,'clientes.json'));
});

app.get('/clientes', function(req, res){
  //res.send('Hola mundo');
  res.sendFile(path.join(__dirname,'clientes.json'));
});

app.get('/clientes/:idcliente', function(req, res){
  res.send('Info del cliente  '+ req.params.idcliente);

});

app.post('/', function(req, res){
  res.send('Hemos recibido su petición!');
});

app.post('/clientes', function(req, res){
  res.send('Cliente añadido');
});

app.put('/clientes', function(req, res){
  res.send('Cliente modificado');
});

app.delete('/clientes', function(req, res){
  res.send('Cliente eliminado');
});
